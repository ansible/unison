# Unison

This Ansible role installs Unison and manages its `.prf` files.

This role never wipes the `~/.unison` folder, because I think it is better to
preserve any existing archives, i.e., non-`*.prf` files.
But note that this role wipes the contents of any non-role `*.prf` files,
i.e., any `.prf` files in `~/.unision/` folder that are *not* managed by this role
will get their contents wiped (but the file itself is not deleted).
This is due to the `sed` command we employ.


## Links and notes

+ https://github.com/bcpierce00/unison
+ https://htmlpreview.github.io/?https://raw.githubusercontent.com/bcpierce00/unison/documentation/unison-manual.html
+ https://github.com/bcpierce00/unison/wiki
+ https://askubuntu.com/questions/1234767/can-i-install-unison-for-ubuntu-18-04-lts-on-ubuntu-20-04-lts
+ https://rclone.org/commands/rclone_copy
+ https://blog.ls-al.com/hiding-passwords-in-scripts
